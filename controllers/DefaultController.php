<?php
namespace app\controllers;

/**
 * DefaultController Class
 *
 * Responsible for displaying, creating and editing tasks.
 *
 * @version 0.1.0
 */

use app\lib\Controller;
use app\models\TaskModel;
use Intervention\Image\ImageManager;

class DefaultController extends Controller
{
    /**
     * Action `Index`
     *
     * Displays all tasks
     *
     * @var object $tasks
     *
     * @return $this
     */

    public function actionIndex()
    {
        $model = new TaskModel();
        $tasks = $model->getAllRows();

        return $this->view->render('index', ['tasks' => $tasks]);
    }

    /**
     * Action `Create`
     *
     * Shows form of the task creation, receives data, validates and save in DB.
     * To load a resize image, use the ImageManager library
     *
     * @var string $username
     * @var string $email
     * @var string $description
     *
     * @return $this
     */
    public function actionCreate()
    {
        if(isset($_POST) && array_key_exists('submit', $_POST)) {
            $username = htmlspecialchars(stripslashes(trim($_POST['username'])));
            $email = htmlspecialchars(stripslashes(trim($_POST['email'])));
            $description = htmlspecialchars(stripslashes(trim($_POST['description'])));

            if(!empty($username) && (!empty($email) || filter_var($email, FILTER_VALIDATE_EMAIL))) {
                $file = '';
                $types = ['image/gif', 'image/png', 'image/jpeg'];

                if(isset($_FILES['img']) && in_array($_FILES['img']['type'], $types)) {

                    $fileName = pathinfo($_FILES['img']['name'], PATHINFO_FILENAME);
                    $fileExtension = pathinfo($_FILES['img']['name'], PATHINFO_EXTENSION);
                    $img = $_FILES['img']['tmp_name'];

                    $file = hash("md5", $fileName) . '_320x240.' . $fileExtension;
                    $file_path = ROOT . "/web/uploads/" . $file;

                    $manager = new ImageManager(['driver' => 'gd']);
                    $manager->make($img)->resize(320, 240, function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save($file_path);
                }

                $model = new TaskModel();
                $model->username = $username;
                $model->email = $email;
                $model->description = $description;
                $model->img = $file;

                if($model->save()) {

                    return $this->view->render('create', ['success' => true]);
                }
            }

            return $this->view->render('create', ['error' => true]);
        }

        return $this->view->render('create');
    }

    /**
     * Action `Edit`
     *
     * Verifies the user's rules to edit, and edits task and status data
     *
     * @var integer $id
     *
     * @return $this
     */
    public function actionEdit()
    {
        if(!array_key_exists('is_auth', $_SESSION) || !$_SESSION["is_auth"] || $_SESSION["login"] != LOGIN) {
            header('Location: /');

            return;
        }

        $id = array_key_exists('id', $_GET) ? $_GET['id'] : '';
        if(!$id) {
            return;
        }

        $model = new TaskModel(['where' => 'id = ' . $id]);
        $model->fetchOne();
        if(!$model) {
            return;
        }

        if(isset($_POST) && array_key_exists('submit', $_POST)) {
            $model->description = htmlspecialchars(stripslashes(trim($_POST['description'])));
            $model->status = (int)$_POST['status'];
            $res = $model->update();

            return $this->view->render('edit', ['task' => $model, 'success' => true]);
        } else if(isset($_POST) && array_key_exists('cancel', $_POST)) {
            header('Location: /');

            return;
        }

        return $this->view->render('edit', ['task' => $model]);
    }

    /**
     * Action `Preview`
     *
     * Display the task preview
     *
     * @var integer $id
     *
     * @return $this
     */
    public function actionPreview()
    {
        $id = array_key_exists('id', $_GET) ? $_GET['id'] : '';
        if(!$id) {
            return;
        }

        $model = new TaskModel(['where' => 'id = ' . $id]);
        $model->fetchOne();
        if(!$model) {
            return;
        }

        return $this->view->render('preview', ['task' => $model]);
    }
}