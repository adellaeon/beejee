<?php

namespace app\controllers;

/**
 * AuthController Class
 *
 * AuthController responsible for login and logout for admin
 *
 * @version 0.1.0
 */

use app\lib\Controller;

class AuthController extends Controller
{
    /**
     * Action `Login`
     *
     * Check the data from form.
     * if form valid, save session and authorize the admin.
     * else send error message in form
     *
     * @return $this
     */
    public function actionLogin()
    {
        if(isset($_POST) && array_key_exists('submit', $_POST)) {
            $_SESSION["is_auth"] = false;

            if(($_POST['login']) && ($_POST['password'])) {
                if(trim($_POST['login']) == LOGIN && trim($_POST['password']) == PASSWORD) {
                    $_SESSION["is_auth"] = true;
                    $_SESSION["login"] = LOGIN;

                    return $this->view->render('login', ['success' => true]);
                } else {
                    return $this->view->render('login', ['error' => true]);
                }
            } else {
                return $this->view->render('login', ['error' => true]);
            }
        }

        return $this->view->render('login');
    }

    /**
     * Action `Logout`
     *
     * Destroy session and user redirect to home page
     *
     * @return $this
     */
    public function actionLogout()
    {
        $_SESSION = [];
        session_destroy();
        header('Location: /');
    }
}


