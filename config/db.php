<?php

try {
    $dbObject = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
    $dbObject->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbObject->exec('SET CHARACTER SET utf8');
} catch(PDOException $e) {
    echo $e->getMessage();
}