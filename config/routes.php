<?php

return [
    'index'   => 'default/index',
    'create'  => 'default/create',
    'edit'    => 'default/edit',
    'preview' => 'default/preview',
    'login'   => 'auth/login',
    'logout'  => 'auth/logout',
];