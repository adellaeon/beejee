<?php

namespace app\models;

/**
 * TaskModel Class
 *
 * @version 0.1.0
 */

use app\lib\Model;

class TaskModel extends Model
{
    public $id;

    public $username;

    public $email;

    public $description;

    public $status;

    public $img;

    public $created_at;

    public function fieldsTable()
    {
        return [
            'id'          => 'Id',
            'username'    => 'Имя',
            'email'       => 'Email',
            'description' => 'Задача',
            'status'      => 'Статус',
            'img'         => 'Изображение',
            'created_at'  => 'Дата создания',
        ];
    }
}