<h1>Редактирование задачи</h1>
<div class="row">
    <div class="col-xs-12">
        <?php if(!empty($success)): ?>
            <div class="alert alert-success" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                Задача отредактрована! Вы можете перейти к <a href="/" class="alert-link">просмотру всех задач</a>.
            </div>
        <?php endif; ?>
        <div class="alert alert-default" role="alert">
            <div><label for="username">Имя: </label> <?=$task->username?></div>
            <div><label for="Email">Email: </label> <?=$task->email?></div>
        </div>
        <?php if($task->img): ?>
            <div class="form-group">
                <label for="img">Изображение: </label>
                <img src="/web/uploads/<?=$task->img;?>" class="img-responsive" alt="">
            </div>
        <?php endif; ?>

        <form method="post" action="/edit?id=<?=$task->id;;?>" enctype="multipart/form-data">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="status" name="status" value="<?=$task->status;?>"> Задача завершена
                </label>
            </div>
            <div class="form-group">
                <label for="description">Задача:</label>
                <textarea class="form-control" id="description" name="description" rows="3"><?=$task->description?></textarea>
            </div>
            <button type="submit" name="cancel" class="btn btn-warning">Отмена</button>
            <button type="submit" name="submit" class="btn btn-success">Сохранить</button>
        </form>
    </div>
</div>