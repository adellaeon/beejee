<h1>Просмотр задачи</h1>
<div class="row">
    <div class="col-xs-12">
        <div><label for="username">Имя: </label> <?=$task->username;?></div>
        <div><label for="email">Email: </label> <?=$task->email;?></div>
        <div><label for="status">Статус: </label>
            <?php if($task->status == 1): ?>
                Завершено
            <?php else: ?>
                Новое
            <?php endif; ?>
        </div>
        <div><label for="created_at">Дата создания: </label> <?=$task->created_at;?></div>
    </div>

    <?php if($task->img): ?>
        <div class="col-xs-8">
            <label for="description">Задача: </label><?=$task->description;?>
        </div>
        <div class="col-xs-4">
            <label for="img">Изображение: </label><img src="/web/uploads/<?=$task->img;?>" class="img-responsive">
        </div>
    <?php else: ?>
        <div class="col-xs-12"><label for="description">Задача: </label><?=$task->description;?></div>
    <?php endif; ?>
</div>

