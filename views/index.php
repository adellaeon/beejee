<h1>Задачи</h1>
<div class="row">
    <div class="col-md-12">
        <table id="tasks" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Имя</th>
                <th>Email</th>
                <th>Задача</th>
                <th>Изображение</th>
                <th>Дата создания</th>
                <th>Статус</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            <?php if($tasks): ?>
                <?php foreach ($tasks as  $val):?>
                    <tr>
                        <td class="dt-center"><?=$val['username'];?></td>
                        <td class="dt-center"><?=$val['email'];?></td>
                        <td class="dt-center"><?=mb_strimwidth($val['description'], 0, 255, "...");?></td>
                        <td class="dt-center">
                            <?php if($val['img']): ?>
                                <img src="/web/uploads/<?=$val['img'];?>" class="img-responsive center-block" alt="" width="170px">
                            <?php endif; ?>
                        </td>
                        <td class="dt-center"><?=$val['created_at'];?></td>
                        <td class="dt-center">
                            <?php if($val['status'] == 1):?>
                                Завершено
                            <?php else: ?>
                                Новое
                            <?php endif; ?>
                        </td>
                        <td class="dt-center">
                            <a href="/preview?id=<?=$val['id'];?>" class="btn btn-info btn-xs btn-block" role="button">Просмотреть</a>
                            <?php if(array_key_exists('is_auth', $_SESSION) && $_SESSION["is_auth"] && $_SESSION["login"] == LOGIN): ?>
                                <a href="/edit?id=<?=$val['id'];?>" class="btn btn-warning btn-xs btn-block" role="button">Редактировать</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif;?>
            </tbody>
        </table>
    </div>
</div>