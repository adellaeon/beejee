<h1>Создание задачи</h1>

<div class="row">
    <div class="col-xs-12">
        <?php if(!empty($success)): ?>
            <div class="alert alert-success" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                Поздравляем, задача создана! Вы можете перейти к <a href="/" class="alert-link">просмотру всех задач</a>.
            </div>
        <?php elseif(!empty($error)): ?>
            <div class="alert alert-warning" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                Пожалуйста, заполните форму
            </div>
        <?php endif; ?>

        <form method="post" action="/create" enctype="multipart/form-data">
            <div class="form-group">
                <label for="username">Имя:</label>
                <input type="text" class="form-control" name="username" id="username">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="email" id="email">
            </div>
            <div class="form-group">
                <label for="description">Задача:</label>
                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
            </div>

            <div class="form-group">
                <label for="img">Изображение</label>
                <input type="file" class="form-control-file" name="img" id="img">
            </div>

            <button type="submit" name="submit" class="btn btn-success btn-lg">Создать задачу</button>
        </form>

        <div style="float: right;margin-top: -35px;">
            <button class="btn btn-default btn-lg" id="preview" data-toggle="modal" data-target="#previewModal">Предварительный просмотр</button>
        </div>
    </div>
</div>

<div id="previewModal" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Предварительный просмотр задачи</h4>
            </div>
            <div class="modal-body">
                <div><label for="previewUsername">Имя: </label> <span id="previewUsername"></span></div>
                <div><label for="previewEmail">Email: </label> <span id="previewEmail"></span></div>
                <div><label for="previewDescription">Задача: </label><span id="previewDescription"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>