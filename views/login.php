<h1>Авторизация</h1>

<div class="row">
    <div class="col-xs-12">
        <?php if(!empty($success)): ?>
            <div class="alert alert-success" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                Здравствуйте, <?=LOGIN;?>! Вы можете перейти к <a href="/" class="alert-link">просмотру и редактированию задач</a>.
            </div>
        <?php elseif(!empty($error)): ?>
            <div class="alert alert-warning" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                Логин и/или пароль введены неверно!
            </div>
        <?php endif; ?>

        <?php if(empty($success)): ?>
            <form method="post" action="/login" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="login">Логин:</label>
                    <input type="text" class="form-control" name="login" id="login">
                </div>
                <div class="form-group">
                    <label for="password">Пароль:</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>

                <button type="submit" name="submit" class="btn btn-success">Войти</button>
            </form>
        <?php endif; ?>
    </div>
</div>