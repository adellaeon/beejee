<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/web/images/favicon.png" type="image/png">
    <link rel="stylesheet" href="/web/css/main.css" type="text/css">

    <title>Sopilko Liubov</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.16/b-1.4.2/r-2.2.0/datatables.min.css"/>

    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.16/b-1.4.2/r-2.2.0/datatables.min.js"></script>

    <script src="/web/js/script.js"></script>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Задачник</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li id="index"><a href="/">Задачи</a></li>
                <li id="create"><a href="/create">Создать задачу</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if(array_key_exists('is_auth', $_SESSION) && $_SESSION["is_auth"] && $_SESSION["login"] == LOGIN): ?>
                    <li>
                        <a href="/logout">
                            Здравствуйте, <?=$_SESSION["login"];?>
                            <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Выйти</a>
                    </li>
                <?php else: ?>
                    <li>
                        <a href="/login"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Войти</a>
                    </li>
                <?php endif; ?>

            </ul>
        </div>
    </div>
</div>
<div style="margin-bottom: 50px"></div>

<div class="container">
    <?=$content?>
</div>

</body>
</html>