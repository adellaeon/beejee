$(function () {
    if (!$('.navbar-nav li').hasClass('active')) {
        var uri = location.href.split('/');
        if (uri.indexOf('create') != -1) {
            $("#create").addClass("active");
        } else {
            $("#index").addClass("active");
        }
    }

    $("#index").click(function () {
        $("#index").addClass("active");
        $("#create").removeClass("active");
    });
    $("#create").click(function () {
        $("#create").addClass("active");
        $("#index").removeClass("active");
    });

});


$(document).ready(function () {
    $("#preview").click(function () {
        $("#previewModal #previewUsername").html($('#username').val());
        $("#previewModal #previewEmail").html($('#email').val());
        $("#previewModal #previewDescription").html($('#description').val());
        $("#myModal").modal('show');

    });

    $("#status").change(function () {
        if (this.checked) {
            $("#status").val(1);
        } else {
            $("#status").val(0);
        }
    });

    $('#tasks').DataTable({
        "pageLength": 3,
        "lengthChange": false,
        "searching": false,
        "language": {
            "info": ""
        },
        fixedColumns: {
            heightMatch: 'none'
        }
    });
});
